#Drone Code
1. The drone mode is activated
2. Drone will look for the enemy(**while avoiding teammates**)
3. If the angle and timing is correct, the drone will attempt the shot
4. If the shot cannot be attempted, the drone will relocate for another attempt
5. If the shot can be attempted, the drone will shoot
6. If the shot hit, the enemy is destroyed
7. If the shot is missed, the enemy will counter, destroying the drone
